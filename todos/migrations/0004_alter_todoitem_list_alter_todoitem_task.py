# Generated by Django 5.0.6 on 2024-05-22 18:57

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("todos", "0003_alter_todoitem_is_completed"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="list",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="items",
                to="todos.todolist",
            ),
        ),
        migrations.AlterField(
            model_name="todoitem",
            name="task",
            field=models.CharField(default=True, max_length=100),
        ),
    ]
